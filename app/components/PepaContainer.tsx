import react from 'react';
import { ReactDOM } from 'react';
import PepaImg from './PepaImg';
import styles from "../page.module.css";

export default function PepaComponent(){
    return(
        <div className={styles.pepaContainer} >
            <PepaImg></PepaImg>
        </div>
    )
}
