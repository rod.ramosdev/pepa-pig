import Image from "next/image";
import profilePic from "../img/E3392015-B550-4326-AFA1-52011CC3167D_1_105_c-removebg-preview (1).png";
import styles from "../page.module.css"

export default function PepaImg(){
    return(
        <Image
            src={profilePic}
            alt="Pepa the Cat."
            width={239} height={359}
            className={styles.pepaContainer}
        />
    )
}